package eu.coatrack.mailsprinkler.backend.repository;

import eu.coatrack.mailsprinkler.api.Campaign;
import eu.coatrack.mailsprinkler.api.Target;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "targets", path = "/targets")
public interface TargetRepository extends PagingAndSortingRepository<Target, Long> {

}
