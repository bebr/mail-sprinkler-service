package eu.coatrack.mailsprinkler.backend.repository;

import eu.coatrack.mailsprinkler.api.Campaign;
import java.util.Date;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "campaigns", path = "/campaigns")
public interface CampaignRepository extends PagingAndSortingRepository<Campaign, Long> {

    public List<Campaign> findByLaunchWhenBefore(Date when);

}
