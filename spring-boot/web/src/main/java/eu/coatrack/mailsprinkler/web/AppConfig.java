package eu.coatrack.mailsprinkler.web;

import eu.coatrack.mailsprinkler.backend.repository.CampaignRepository;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class AppConfig {

    private static final Logger log = LoggerFactory.getLogger(AppConfig.class);

    @Autowired
    private CampaignRepository campaignRepository;

    @Scheduled(cron = "*/5 * * * * *")
    public void send() throws MessagingException {
        log.info("**** Start Campaigns Laungh");

        campaignRepository.findByLaunchWhenBefore(new Date()).forEach((campaign) -> {

            log.info("**** Start Campaign Laungh:" + campaign.getName());

            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setHost(campaign.getHost());
            mailSender.setPort(campaign.getPort());
            mailSender.setProtocol(campaign.getProtocol());

            mailSender.setUsername(campaign.getUsername());
            mailSender.setPassword(campaign.getPassword());

            Properties props = mailSender.getJavaMailProperties();
            props.put("mail.transport.protocol", campaign.getProtocol());
            props.put("mail.smtp.auth", campaign.isMailSmtpAuth());
            props.put("mail.smtp.starttls.enable", campaign.isMailSmtpStarttlsEnable());
            props.put("mail.debug", campaign.isMailDebug());

            campaign.getTarget().forEach((target) -> {
                try {
                    MimeMessage message = mailSender.createMimeMessage();
                    MimeMessageHelper helper = new MimeMessageHelper(message, true);
                    helper.setTo(target.getMail());
                    helper.setFrom(campaign.getMailFrom());
                    helper.setSubject(campaign.getMailSubject());
                    helper.setText(campaign.getMailSubject(), campaign.isIsHTMLText());
                    mailSender.send(message);
                } catch (MessagingException ex) {
                    java.util.logging.Logger.getLogger(AppConfig.class.getName()).log(Level.SEVERE, null, ex);
                    campaign.getError().add(ex.getMessage());
                    campaignRepository.save(campaign);
                }
            });

        });

    }

}
