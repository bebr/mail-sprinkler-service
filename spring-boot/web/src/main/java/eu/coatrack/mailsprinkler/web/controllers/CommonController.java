package eu.coatrack.mailsprinkler.web.controllers;

import eu.coatrack.mailsprinkler.api.Campaign;
import eu.coatrack.mailsprinkler.api.Target;
import eu.coatrack.mailsprinkler.backend.repository.CampaignRepository;
import eu.coatrack.mailsprinkler.backend.repository.TargetRepository;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/admin")
public class CommonController {

    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private CampaignRepository campaignRepository;

    @RequestMapping(value = "/sample", produces = "application/json")
    @ResponseBody
    public Campaign home(@RequestParam("user") String user, @RequestParam("password") String password) {

        Campaign campaign = new Campaign();
        campaign.setDescription("This is a test");
        campaign.setHost("smtp.gmail.com");
        campaign.setIsHTMLText(true);
        campaign.setLaunchWhen(new Date());
        campaign.setMailDebug(true);
        campaign.setMailFrom("registration@coatrack.eu");
        campaign.setMailSmtpAuth(true);
        campaign.setMailSmtpStarttlsEnable(true);
        campaign.setMailSubject("Mail Test");
        campaign.setMailText("This is the text of the Test");
        campaign.setName("name");
        campaign.setOwner("owner");
        campaign.setPassword(password);
        campaign.setPort(587);
        campaign.setProtocol("smtp");
        campaign.setUsername(user);

        Target target = new Target();
        target.setMail("fpd.judo@gmail.com");

        target.setCampaign(campaign);
        campaign.getTarget().add(target);

        Campaign savedCampaign = campaignRepository.save(campaign);

        return savedCampaign;

    }

}
