package eu.coatrack.mailsprinkler.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication       
@EnableJpaRepositories(basePackages = {"eu.coatrack.mailsprinkler.backend.repository"})

@ComponentScan(basePackages = {"eu.coatrack.mailsprinkler.web","eu.coatrack.mailsprinkler.web.controllers"})
@EntityScan(basePackages = {"eu.coatrack.mailsprinkler.api"})
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }

}
