# Mail Sprinkler Service

The aim of this service is to provide a reusable and specialized service for sending mails. To send a mail is a common and repeatable operation of several app and portals. Every of these apps overcome similiar steps and issues like smtp configuration or sending progresively the mails in case of large amount of targets.


## Strengths

Mail Sprinkler Service (MSS) offers an asynchronous and specialize way to send a small or large amount of messages to your list of maillist.
MSS offers a rest api where you can define your mail campaign and schedule when must be launched. Therefore you can delegate that functionality of your app/portal and forget typical issues like the :

 - Maximum messages per day
 - Maximum messages auto-forwarded
 - Maximum auto-forward mail filters
 - Recipients per message
 - Total receipients per day
 - etc...

## Build and Deployment
In orther to build the project, you only need to clone from bitbucket repositoy and execute:
**mvn clean install**
In order to deploy you must execute:
**java -jar spring-boot/web/target/*.jar**

## Project Status
This is just a POC and initial commit where we can test the usability of the project and detect if it is really neccesary in the Market.

## UML diagrams

TODO Keep it in order to write down in future

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/). For example, this will produce a sequence diagram:

```mermaid
sequenceDiagram
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```

And this will produce a flow chart:

```mermaid
graph LR
A[Square Rect] -- Link text --> B((Circle))
A --> C(Round Rect)
B --> D{Rhombus}
C --> D
```